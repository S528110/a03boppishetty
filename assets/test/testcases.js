
QUnit.test( "Quantity number", function( assert ) {
  assert.strictEqual(Test.checkFunction(12),"correct", "Should return correct");
  assert.strictEqual(Test.checkFunction(""),"Qunatity cannont be empty", "Qunatity cannont be empty");
  assert.strictEqual(Test.checkFunction("test"),"Quantity cannot be string", "Quantity cannot be string");
  assert.strictEqual(Test.checkFunction(2),"correct", "Works correctly");
  assert.strictEqual(Test.checkFunction(-12),"Qunatity cannot be negative", "Qunatity cannot be negative");
});